use std::ops::Add;
use std::string::ToString;
use nannou::{draw, color::named, prelude::*};

trait Nannou {
    fn display(&self, draw: &draw::Draw);
    fn update(&mut self);
}

#[derive(Debug, Copy, Clone, PartialEq)]
pub struct Point {
    pub x: f32,
    pub y: f32,
}

impl Point {
    pub fn new(x: f32, y: f32) -> Self {
        Point { x, y }
    }

    pub fn norm(self) -> f32 {
        (self.x.powi(2) + self.y.powi(2)).sqrt()
    }

    pub fn rand() -> Self {
        Point::default()
    }
}

impl Default for Point {
    fn default() -> Self {
        Point::new(0_f32, 0_f32)
    }
}

impl Add for Point {
    type Output = Self;

    fn add(self, _rhs: Point) -> Self {
        Self {
            x: self.x + _rhs.x,
            y: self.y + _rhs.y,
        }
    }
}

pub struct Boyd {
    pub location: Point,
    velocity: Point,
}

impl Boyd {
    pub fn new(x: f32, y: f32) -> Self {
        Boyd { location: Point::new(x, y), velocity: Point::new(x, y) }
    }

    pub fn set_velocity(mut self, v: Point) -> Self {
        self.velocity = v;
        self
    }
}

impl Default for Boyd {
    fn default() -> Self {
        Boyd::new(0_f32, 0_f32)
    }
}
