use nannou::{color::named, prelude::*};
use rand::prelude::*;
//use clap::StructOpt;
use clap::Parser;

#[macro_use]
extern crate lazy_static;

#[derive(Parser, Debug)]
#[clap(name = "flock")]
/// A flocking simulation
pub struct Opt {
    /// Set number of boyds on simulation
    #[clap(short, long, default_value = "4")]
    boyds: u8,
}

lazy_static! {
    pub static ref OPT: Opt = Opt::parse();
}


use boyd::{Boyd};

struct Model {
    _window: window::Id,
    pub boyds: Vec<Boyd>,
}

fn model(app: &App) -> Model {
    let _window = app.new_window().view(view).build().unwrap();
    Model {
        _window: _window,
        boyds: vec![Boyd::new(0_f32, 0_f32), Boyd::new(100_f32, 200_f32)]
    }
}

fn update(_app: &App, _model: &mut Model, _update: Update) {
}

fn view(app: &App, _model: &Model, frame: Frame) {
    let draw = app.draw();
    draw.background().color(WHITE);
    for boyd in _model.boyds.iter() {
        draw.ellipse()
            .color(STEELBLUE)
            .x_y(boyd.location.x, boyd.location.y);
    }
    draw.to_frame(app, &frame).unwrap();
}

fn main() {
    println!("{:?}", OPT.boyds);
    nannou::app(model).update(update).run();
}
